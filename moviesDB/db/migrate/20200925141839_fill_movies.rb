class FillMovies < ActiveRecord::Migration
    def up

        file = JSON.parse(File.read('./data/movies.json'))
        file.each do |m|
            new_movie = Movie.create(
                title: m["title"],
                year: m["year"],
                poster: m["poster"],
                contentRating: m["contentRating"],
                duration: m["duration"],
                releaseDate: m["releaseDate"].to_date,
                averageRating: m["averageRating"],
                originalTitle: m["originalTitle"],
                storyline: m["storyline"],
                imdbRating: m["imdbRating"],
                posterURL: m["posterurl"]
            )

            m["genres"].each do |g|
                new_movie.genres.create(name: g)
            end

            m["ratings"].each do |r|
                new_movie.ratings.create(value: r)
            end

            m["actors"].each do |a|
                new_movie.actors.create(name: a)
            end
            
        end
    end

    def down
        Movie.delete_all
    end
end
