class CreateMoviesTable < ActiveRecord::Migration
    def change
        create_table :movies do |t|
            t.string :title
            t.integer :year
            t.string :poster
            t.float :contentRating
            t.string :duration
            t.date :releaseDate
            t.float :averageRating
            t.string :originalTitle
            t.text :storyline
            t.float :imdbRating
            t.string :posterURL
        end

        create_table :genres do |t|
            t.belongs_to :movie
            t.string :name
        end

        create_table :ratings do |t|
            t.belongs_to :movie
            t.float :value
        end

        create_table :actors do |t|
            t.belongs_to :movie
            t.string :name
        end
    end
end
