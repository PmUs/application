class GenresController < ApplicationController
    skip_before_action :verify_authenticity_token

    def index
        @movie = Movie.find(params[:id])
    end

    def delete
        @genre = Genre.find(params[:id])
        @movie_id = @genre.movie_id
        @genre.destroy
        redirect_to genres_path(@movie_id)
    end

    def create
        @movie = Movie.find(params[:id])
        @movie.genres.create(name: params.require("name")) if params[:name].present?
        redirect_to genres_path(@movie.id)
    end
end