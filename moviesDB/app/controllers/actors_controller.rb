class ActorsController < ApplicationController
    skip_before_action :verify_authenticity_token

    def index
        @movie = Movie.find(params[:id])
    end

    def delete
        @actor = Actor.find(params[:id])
        @movie_id = @actor.movie_id
        @actor.destroy
        redirect_to actors_path(@movie_id)
    end

    def create
        @movie = Movie.find(params[:id])
        @movie.actors.create(name: params.require("name")) if params[:name].present?
        redirect_to actors_path(@movie.id)
    end
end