require "rubygems/text"

class PagesController < ApplicationController
    
    def home
    end

    def search
        @title = params[:title]

        if (@title == nil)
            @title = ''
        end

        @title = @title.downcase
        
        lev_dist = Class.new.extend(Gem::Text).method(:levenshtein_distance)

        @list = Movie.all.sort_by{|m| m.title != nil ? lev_dist.call(m.title.downcase, @title): lev_dist.call('', @title)}
    end
end