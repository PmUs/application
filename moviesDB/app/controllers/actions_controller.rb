
class ActionsController < ApplicationController
    
    skip_before_action :verify_authenticity_token
    
    def delete_movie
        begin
            @movie = Movie.find(params[:id])
            @movie.destroy
            render :json => "movie deleted"
        rescue ActiveRecord::RecordNotFound
            render :json => "movie not found"
        end
    end

    def create_movie
        @movie = Movie.new()

        @movie.title = params[:title]
        @movie.year = params[:year]
        @movie.poster = params[:poster]
        @movie.contentRating = params[:contentRating]
        @movie.duration = params[:duration]
        @movie.releaseDate = params[:releaseDate]
        @movie.originalTitle = params[:originalTitle]
        @movie.storyline = params[:storyline]
        @movie.imdbRating = params[:imdbRating]
        @movie.posterURL = params[:posterurl]
        @movie.averageRating =params[:averageRating]

        @movie.save
        render :json => @movie.id
    end
end
