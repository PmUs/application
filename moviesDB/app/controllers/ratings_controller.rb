class RatingsController < ApplicationController
    skip_before_action :verify_authenticity_token

    def index
        @movie = Movie.find(params[:id])
    end

    def delete
        @rating = Rating.find(params[:id])
        @movie_id = @rating.movie_id
        @rating.destroy
        redirect_to ratings_path(@movie_id)
    end

    def create
        @movie = Movie.find(params[:id])
        @movie.ratings.create(value: params.require("value")) if params[:value].present?
        redirect_to ratings_path(@movie.id)
    end
end