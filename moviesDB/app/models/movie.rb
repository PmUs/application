class Movie < ApplicationRecord
    has_many :genres, dependent: :destroy
    has_many :ratings, dependent: :destroy
    has_many :actors, dependent: :destroy
end