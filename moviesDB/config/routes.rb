Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  #home
  get '/', to: 'pages#home', as: 'home'
  post '/', to: 'pages#search'


  # genres
  get  '/movies/:id/genres/', to: 'genres#index', as: 'genres'
  post '/movies/:id/genres/', to: 'genres#create'
  delete '/genres/:id', to: 'genres#delete', as: 'genre'

  # actors
  get  '/movies/:id/actors/', to: 'actors#index', as: 'actors'
  post '/movies/:id/actors/', to: 'actors#create'
  delete '/actors/:id', to: 'actors#delete', as: 'actor'

  # ratings
  get  '/movies/:id/ratings/', to: 'ratings#index', as: 'ratings'
  post '/movies/:id/ratings/', to: 'ratings#create'
  delete '/ratings/:id', to: 'ratings#delete', as: 'rating'

  # movies
  resources :movies

  # api
  post '/api/1/movies', to:'actions#create_movie'
  delete '/api/1/movies/:id', to:'actions#delete_movie'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
